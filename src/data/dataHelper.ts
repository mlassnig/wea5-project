export class DataHelper {
  private lorem: string =
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean iaculis convallis felis. Fusce est lacus, accumsan a tempus in, molestie sed est. Duis at sapien scelerisque, vestibulum nisl ut, dignissim ligula. Sed ac fringilla sapien. Proin ac tempus libero. Sed vitae auctor augue. Vestibulum pretium, urna nec posuere tempor, ipsum turpis fringilla velit, id volutpat dolor urna at felis. Praesent placerat velit quis imperdiet porttitor. Curabitur urna metus, sodales sed varius vel, condimentum ac augue. Nullam vitae scelerisque ex.';
  private images: string[] = [
    'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcR2e2BHuL7eTyeoVyIwwp8jQNtLFp5lAafKQERxLu869YIHRW9L',
    'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcR9_CURKlNvarzHf7J3Q4MjAnDQzdKe7XgaVGjQDGJw0-LpsvTk',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRoTgSPueDIcI6b6SYpE2YnT45XASKCRxLnzY_NCKJ6-3fVtJKC',
    'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTL50EWyClxgJylvmheWC6UX4xeZYNfqjMc8VTdQCeQwbKO64_Z',
    'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSbJ890Bo1eUwOt1l-9Rf30lbs-O-djj3XWV-nkCBbPTu4TeSLq',
    'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSFjKwkEgropMVZAIFXUlImtxT2_lKjTzp1dMhQvlAhd9Hf0moa',
    'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT7E39vtLbHq452pSfY47iDioWHwgmL-RGi0M9hqMuwxw-oAE7S',
    'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQJE1dnNvGClEw9FIjKj7l_VAjeLajWoZNpHUx2XtO0eAcpS4H3',
  ];
  getLorem(charCount: number): string {
    if (charCount > this.lorem.length) {
      charCount = this.lorem.length;
    }
    return this.lorem.slice(0, charCount);
  }

  getRandomChar(length: number): string {
    let result = '';
    let characters = 'abcdefghijklmnopqrstuvwxyz';
    for (var i = 0; i < length; i++) {
      result += characters.charAt(
        Math.floor(Math.random() * characters.length)
      );
    }
    return result;
  }

  getImage(): string {
    return this.images[Math.floor(Math.random() * this.images.length)];
  }
}
