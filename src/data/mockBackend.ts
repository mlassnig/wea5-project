import { Movie } from 'src/app/interfaces/movie.type';
import { DataHelper } from './dataHelper';
import { Actor } from 'src/app/interfaces/actor.type';
import { Genre } from 'src/app/interfaces/genre.type';
import * as faker from 'faker';
import { CinemaHall } from 'src/app/interfaces/cinemaHall.type';
import { SeatRow } from 'src/app/interfaces/seatRow.type';
import { RowCategory } from 'src/app/interfaces/rowCategory.type';
import { Seat } from 'src/app/interfaces/seat.type';
import * as _ from 'lodash';
import { Schedule } from 'src/app/interfaces/schedule.type';

export class MockBackend {
  private dataHelper: DataHelper = new DataHelper();

  getMovies(count: number): Movie[] {
    let movies: Movie[] = new Array<Movie>();
    for (let i = 0; i < count; i++) {
      movies.push({
        id: i + 1,
        title: this.dataHelper.getRandomChar(Math.floor(Math.random() * 25)),
        length: Math.floor(Math.random() * 200),
        image: this.dataHelper.getImage(),
        trailerLink: 'https://www.youtube.com/watch?v=LkG-5sYVQIA',
        description: this.dataHelper.getLorem(Math.floor(Math.random() * 500)),
        actors: this.getActors(Math.floor(Math.random() * 9) + 1),
        genres: this.getGenres(Math.floor(Math.random() * 9) + 1),
      });
    }
    return movies;
  }

  getActors(count: number): Actor[] {
    let actors: Actor[] = new Array<Actor>();

    for (let i = 0; i < count; i++) {
      actors.push({
        id: i + 1,
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      });
    }

    return actors;
  }

  getGenres(count: number): Genre[] {
    let genreTypes: Genre[] = [
      { name: 'Action' },
      { name: 'Animation' },
      { name: 'Comedy' },
      { name: 'Crime' },
      { name: 'Drama' },
      { name: 'Experimental' },
      { name: 'Fantasy' },
      { name: 'Historical' },
      { name: 'Horror' },
      { name: 'Romance' },
      { name: 'Science Fiction' },
      { name: 'Thriller' },
      { name: 'Western' },
    ];
    let genres: Genre[] = _.take<Genre>(genreTypes, count);
    return genres;
  }

  getCinemaHall(numOfRows: number, numOfSeatsPerRow: number): CinemaHall {
    let rows: SeatRow[] = new Array<SeatRow>();
    for (let i = 0; i < numOfRows; i++) {
      let seats: Seat[] = new Array<Seat>();
      for (let j = 0; j < numOfSeatsPerRow; j++) {
        seats.push({ id: j });
      }
      rows.push({
        rowCategory: this.getRowCategories(1)[0],
        seats: seats,
      });
    }
    let cinemaHall: CinemaHall = {
      id: Math.floor(Math.random() * 100000),
      name: faker.company.companyName(),
      rows,
    };

    return cinemaHall;
  }

  getRowCategories(count: number): RowCategory[] {
    let categories: RowCategory[] = new Array<RowCategory>();
    let id: number = 0;
    for (let i = 0; i < count; i++) {
      categories.push({
        id: id++,
        name: faker.company.companyName(),
        price: Math.random() * 20,
      });
    }
    return categories;
  }

  getSchedules(count: number): Schedule[] {
    let schedules: Schedule[] = new Array<Schedule>();
    const monthInMillis: number = 2.592e9;
    for (let i = 0; i < count; i++) {
      schedules.push({
        cinemaHall: this.getCinemaHall(
          Math.floor(Math.random() * 10) + 10,
          Math.floor(Math.random() * 10) + 10
        ),
        movie: this.getMovies(1)[0],
        startTime: new Date(
          _.now() + Math.random() * (_.now() + monthInMillis - _.now())
        ).getTime(),
      });
    }
    return schedules;
  }
}
