import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CinemaProgramComponent } from './cinema-program/cinema-program.component';
import { MovieLibraryComponent } from './movie-library/movie-library.component';
import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { AdminAreaComponent } from './admin-area/admin-area.component';
import { AdminMovieDetailsComponent } from './admin-movie-details/admin-movie-details.component';
import { AdminScheduleDetailsComponent } from './admin-schedule-details/admin-schedule-details.component';
import { AdminCinemaHallDetailsComponent } from './admin-cinema-hall-details/admin-cinema-hall-details.component';
import { AdminCategoryDetailsComponent } from './admin-category-details/admin-category-details.component';
import { LoginGuardGuard } from './login-guard.guard';

const routes: Routes = [
  { path: '', component: CinemaProgramComponent },
  { path: 'library', component: MovieLibraryComponent },
  {
    path: 'admin',
    component: AdminAreaComponent,
    canActivate: [LoginGuardGuard],
  },
  {
    path: 'admin/movieDetails/:id',
    component: AdminMovieDetailsComponent,
    canActivate: [LoginGuardGuard],
  },
  {
    path: 'admin/cinemaHallDetails/:id',
    component: AdminCinemaHallDetailsComponent,
    canActivate: [LoginGuardGuard],
  },
  {
    path: 'admin/categoryDetails/:id',
    component: AdminCategoryDetailsComponent,
    canActivate: [LoginGuardGuard],
  },
  {
    path: 'admin/scheduleDetails',
    component: AdminScheduleDetailsComponent,
    canActivate: [LoginGuardGuard],
  },
  { path: 'movie/:id', component: MovieDetailsComponent },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
