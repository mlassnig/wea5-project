import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BackendService } from '../backend.service';
import { Actor } from '../interfaces/actor.type';
import { MovieDto } from '../interfaces/dto/movie.dto.type';
import { Genre } from '../interfaces/genre.type';

@Component({
  selector: 'app-admin-movie-details',
  templateUrl: './admin-movie-details.component.html',
  styleUrls: ['./admin-movie-details.component.scss'],
})
export class AdminMovieDetailsComponent implements OnInit {
  isInitialized: boolean = false;
  isActionCreate: boolean = true;
  inputMovie: MovieDto | undefined;
  movie: MovieDto = undefined!;
  actorList: Actor[] = new Array<Actor>();
  genreList: Genre[] = new Array<Genre>();

  form: FormGroup;
  errors: string[] = new Array<string>();

  readonly HTTP_PATTERN: string =
    '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
  readonly MAX_LENGTH: number = Math.pow(2, 31) - 1;

  constructor(
    formBuilder: FormBuilder,
    private backendService: BackendService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.form = formBuilder.group({
      title: [
        '',
        [
          Validators.required,
          Validators.pattern('^[A-Za-z ]+$'),
          Validators.minLength(2),
          Validators.maxLength(196), //https://www.imdb.com/list/ls064443882/
        ],
      ],
      description: ['', [Validators.required, Validators.maxLength(500)]],
      length: [
        '',
        [
          Validators.required,
          Validators.min(0),
          Validators.max(this.MAX_LENGTH),
        ],
      ],
      image: ['', [Validators.required, Validators.pattern(this.HTTP_PATTERN)]],
      trailerLink: [
        '',
        [Validators.required, Validators.pattern(this.HTTP_PATTERN)],
      ],
      actors: ['', [Validators.required]],
      genres: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    let id: number = parseInt(this.route.snapshot.paramMap.get('id') || '0');
    let isMovieInitialized: boolean = false;
    let isActorsInitialized: boolean = false;
    let isGenresInitialized: boolean = false;
    if (id == 0) {
      isMovieInitialized = true;
      this.isInitialized =
        isMovieInitialized && isActorsInitialized && isGenresInitialized;
    }
    if (id > 0) {
      this.backendService.getMovieById(id).subscribe((data: MovieDto) => {
        this.inputMovie = data;
        isMovieInitialized = true;
        this.isInitialized =
          isMovieInitialized && isActorsInitialized && isGenresInitialized;
        if (this.inputMovie.movie) {
          this.form.controls['title'].setValue(this.inputMovie.movie.title);
          this.form.controls['description'].setValue(
            this.inputMovie.movie.description
          );
          this.form.controls['length'].setValue(this.inputMovie.movie.length);
          this.form.controls['image'].setValue(this.inputMovie.movie.image);
          this.form.controls['trailerLink'].setValue(
            this.inputMovie.movie.trailerLink
          );
          this.isActionCreate = false;
        } else {
          this.inputMovie = undefined;
        }
      });
    }

    this.backendService.getActors().subscribe((data) => {
      this.actorList = data;

      isActorsInitialized = true;
      this.isInitialized =
        isMovieInitialized && isActorsInitialized && isGenresInitialized;
    });
    this.backendService.getGenres().subscribe((data) => {
      this.genreList = data;

      isGenresInitialized = true;
      this.isInitialized =
        isMovieInitialized && isActorsInitialized && isGenresInitialized;
    });
  }

  onSubmit(): void {
    if (this.form.valid) {
      this.movie = {
        actors: this.form.controls['actors'].value,
        genres: this.form.controls['genres'].value,
        movie: {
          id: 0,
          title: this.form.controls['title'].value,
          description: this.form.controls['description'].value,
          length: this.form.controls['length'].value,
          image: this.form.controls['image'].value,
          trailerLink: this.form.controls['trailerLink'].value,
        },
      };
      if (!this.isActionCreate) {
        this.movie.movie.id = this.inputMovie?.movie.id || 0;
      }

      if (this.isActionCreate) {
        this.backendService.insertMovie(this.movie).subscribe((id) => {
          this.router.navigate(['/movie/' + id]);
        });
      } else {
        this.backendService.updateMovie(this.movie).subscribe(() => {
          this.router.navigate(['/movie/' + this.movie.movie.id]);
        });
      }
    }
  }
}
