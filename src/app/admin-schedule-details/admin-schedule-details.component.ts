import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BackendService } from '../backend.service';
import { DataStoreService } from '../data-store.service';
import { CinemaHallDto } from '../interfaces/dto/cinemaHall.dto.type';
import { MovieDto } from '../interfaces/dto/movie.dto.type';
import { ScheduleDto } from '../interfaces/dto/schedule.dto.type';
import { Schedule } from '../interfaces/schedule.type';
import * as _ from 'lodash';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-schedule-details',
  templateUrl: './admin-schedule-details.component.html',
  styleUrls: ['./admin-schedule-details.component.scss'],
})
export class AdminScheduleDetailsComponent implements OnInit {
  isInitialized: boolean = false;
  isActionCreate: boolean = true;
  errors: string[] = new Array<string>();
  form: FormGroup;
  schedule: ScheduleDto;
  movies: MovieDto[] = new Array<MovieDto>();
  cinemaHalls: CinemaHallDto[] = new Array<CinemaHallDto>();
  constructor(
    formBuilder: FormBuilder,
    private backendService: BackendService,
    private dataStore: DataStoreService,
    private router: Router
  ) {
    this.schedule = undefined!;

    this.form = formBuilder.group({
      startTime: ['', [Validators.required]],
      movie: ['', [Validators.required]],
      cinemaHall: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.schedule = <ScheduleDto>this.dataStore.get('schedule');

    if (this.schedule !== undefined) {
      this.isActionCreate = false;
    }
    this.dataStore.set(undefined, 'schedule');

    let isMoviesInitialized: boolean = false;
    let isCinemaHallsInitialized: boolean = false;
    this.backendService.getMovies().subscribe((data) => {
      this.movies = data;
      isMoviesInitialized = true;
      this.isInitialized = isCinemaHallsInitialized && isMoviesInitialized;
    });

    this.backendService.getCinemaHalls().subscribe((data) => {
      this.cinemaHalls = data;
      isCinemaHallsInitialized = true;
      this.isInitialized = isCinemaHallsInitialized && isMoviesInitialized;
    });

    if (!this.isActionCreate) {
      this.form.controls['startTime'].setValue(
        this.schedule.schedule.startTime
      );
      let filterMovie = _.filter(this.movies, (m) => {
        return m.movie.id === this.schedule.schedule.movieRef.id;
      });

      this.form.controls['movie'].setValue(this.schedule.schedule.movieRef);
      this.form.controls['cinemaHall'].setValue(
        this.schedule.schedule.cinemaHallRef
      );
    }
  }

  onSubmit(): void {
    if (this.form.valid) {
      let newSchedule: ScheduleDto = {
        schedule: {
          cinemaHallRef: this.form.controls['cinemaHall'].value.cinemaHall,
          movieRef: this.form.controls['movie'].value.movie,
          startTime: this.form.controls['startTime'].value,
        },
        percentageTakenSeats: 0,
      };

      if (this.isActionCreate) {
        this.backendService.insertSchedule(newSchedule).subscribe(
          () => {
            this.router.navigate(['/admin']);
          },
          (error) => {
            this.errors.push(error.error);
          }
        );
      } else {
        this.backendService
          .updateSchedule(this.schedule, newSchedule)
          .subscribe(
            () => {
              this.router.navigate(['/admin']);
            },
            (error) => {
              this.errors.push(error.error);
            }
          );
      }
    }
  }
}
