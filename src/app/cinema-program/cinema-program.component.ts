import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';
import { ScheduleDto } from '../interfaces/dto/schedule.dto.type';
import * as _ from 'lodash';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'cinema-program',
  templateUrl: './cinema-program.component.html',
  styleUrls: ['./cinema-program.component.scss'],
})
export class CinemaProgramComponent implements OnInit {
  schedules: ScheduleDto[] = new Array<ScheduleDto>();
  shownSchedules: ScheduleDto[] = new Array<ScheduleDto>();
  filterType: string[];
  isInitialized: boolean = false;
  form: FormGroup;
  constructor(
    private backendService: BackendService,
    formBuilder: FormBuilder,
    private authenticationService: AuthenticationService
  ) {
    this.form = formBuilder.group({
      search: ['', []],
      filterType: ['', []],
    });

    this.filterType = ['title', 'description'];
    this.form.controls['filterType'].setValue(this.filterType[0]);
  }

  async ngOnInit(): Promise<void> {
    this.backendService.getSchedules().subscribe((data: ScheduleDto[]) => {
      this.schedules = data;
      this.searchChanged();
      this.isInitialized = true;
    });
  }

  searchChanged(): void {
    let searchTerm: string = this.form.controls['search'].value;
    let filterType: string = this.form.controls['filterType'].value;

    if (!searchTerm || searchTerm === '') {
      this.shownSchedules = this.schedules;
    } else {
      if (filterType === 'title') {
        this.filterTitle(searchTerm);
      } else if (filterType === 'description') {
        this.filterDescription(searchTerm);
      }
    }
  }
  filterDescription(searchTerm: string) {
    this.shownSchedules = _.filter(this.schedules, (s) => {
      return s.schedule.movieRef.description
        .toUpperCase()
        .includes(searchTerm.toUpperCase());
    });
  }

  filterTitle(searchTerm: string) {
    this.shownSchedules = _.filter(this.schedules, (s) => {
      return s.schedule.movieRef.title
        .toUpperCase()
        .includes(searchTerm.toUpperCase());
    });
  }
}
