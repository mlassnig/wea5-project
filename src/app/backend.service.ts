import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MovieDto } from './interfaces/dto/movie.dto.type';
import { BackendUrls } from './backendurls';
import { ScheduleDto } from './interfaces/dto/schedule.dto.type';
import { Actor } from './interfaces/actor.type';
import { leadingComment } from '@angular/compiler';
import { Genre } from './interfaces/genre.type';
import { CinemaHall } from './interfaces/cinemaHall.type';
import { CinemaHallDto } from './interfaces/dto/cinemaHall.dto.type';
import { RowCategory } from './interfaces/rowCategory.type';

@Injectable({
  providedIn: 'root',
})
export class BackendService {
  constructor(private http: HttpClient) {}

  //-------------------Movies-------------------

  getMovies(): Observable<MovieDto[]> {
    return this.http.get<MovieDto[]>(BackendUrls.BASE_URL + BackendUrls.MOVIES);
  }

  getMovieById(id: number): Observable<MovieDto> {
    return this.http.get<MovieDto>(
      BackendUrls.BASE_URL + BackendUrls.MOVIES + '/movie/' + id
    );
  }

  insertMovie(movie: MovieDto): Observable<number> {
    return this.http.post<number>(
      BackendUrls.BASE_URL + BackendUrls.MOVIES,
      movie
    );
  }

  updateMovie(movie: MovieDto): Observable<any> {
    return this.http.put<number>(
      BackendUrls.BASE_URL + BackendUrls.MOVIES + '/movie',
      movie
    );
  }

  deleteMovie(id: number): Observable<any> {
    return this.http.delete<number>(
      BackendUrls.BASE_URL + BackendUrls.MOVIES + '/movie/' + id
    );
  }

  //-------------------Schedules-------------------

  getSchedules(): Observable<ScheduleDto[]> {
    return this.http.get<ScheduleDto[]>(
      BackendUrls.BASE_URL + BackendUrls.SCHEDULES
    );
  }

  getSchedulesForMovie(id: number): Observable<ScheduleDto[]> {
    return this.http.get<ScheduleDto[]>(
      BackendUrls.BASE_URL + BackendUrls.SCHEDULES + '/movie/' + id
    );
  }

  updateSchedule(
    oldSchedule: ScheduleDto,
    newSchedule: ScheduleDto
  ): Observable<ScheduleDto> {
    const httpOptions = {
      headers: new HttpHeaders({
        startTime: oldSchedule.schedule.startTime + '',
        cinemaHallId: oldSchedule.schedule.cinemaHallRef.id + '',
        movieId: oldSchedule.schedule.movieRef.id + '',
      }),
    };

    return this.http.put<ScheduleDto>(
      BackendUrls.BASE_URL + BackendUrls.SCHEDULES + '/schedule/',
      newSchedule,
      httpOptions
    );
  }

  insertSchedule(schedule: ScheduleDto): Observable<number> {
    return this.http.post<number>(
      BackendUrls.BASE_URL + BackendUrls.SCHEDULES,
      schedule
    );
  }

  deleteSchedule(schedule: ScheduleDto): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        startTime: schedule.schedule.startTime + '',
        cinemaHallId: schedule.schedule.cinemaHallRef.id + '',
        movieId: schedule.schedule.movieRef.id + '',
      }),
    };

    return this.http.delete<number>(
      BackendUrls.BASE_URL + BackendUrls.SCHEDULES + '/schedule/',
      httpOptions
    );
  }

  //-------------------CinemaHalls-------------------
  getCinemaHalls(): Observable<CinemaHallDto[]> {
    return this.http.get<CinemaHallDto[]>(
      BackendUrls.BASE_URL + BackendUrls.CINEMAHALLS
    );
  }

  getCinemaHallById(id: number): Observable<CinemaHallDto> {
    return this.http.get<CinemaHallDto>(
      BackendUrls.BASE_URL + BackendUrls.CINEMAHALLS + '/cinemaHall/' + id
    );
  }

  deleteCinemaHall(id: number): Observable<any> {
    return this.http.delete(
      BackendUrls.BASE_URL + BackendUrls.CINEMAHALLS + '/cinemaHall/' + id
    );
  }

  insertCinemaHall(cinemaHall: CinemaHallDto) {
    return this.http.post(
      BackendUrls.BASE_URL + BackendUrls.CINEMAHALLS,
      cinemaHall
    );
  }

  updateCinemaHall(cinemaHall: CinemaHallDto): Observable<number> {
    return this.http.put<number>(
      BackendUrls.BASE_URL + BackendUrls.CINEMAHALLS + '/cinemaHall',
      cinemaHall
    );
  }

  //-------------------Actors-------------------

  getActors(): Observable<Actor[]> {
    return this.http.get<Actor[]>(BackendUrls.BASE_URL + BackendUrls.ACTORS);
  }

  //-------------------Genres-------------------

  getGenres(): Observable<Genre[]> {
    return this.http.get<Genre[]>(BackendUrls.BASE_URL + BackendUrls.GENRES);
  }

  //-------------------Categories-------------------

  getCategories(): Observable<RowCategory[]> {
    return this.http.get<RowCategory[]>(
      BackendUrls.BASE_URL + BackendUrls.CATEGORIES
    );
  }

  getCategoryById(id: number): Observable<RowCategory> {
    return this.http.get<RowCategory>(
      BackendUrls.BASE_URL + BackendUrls.CATEGORIES + '/category/' + id
    );
  }

  insertCategory(category: RowCategory) {
    return this.http.post<any>(BackendUrls.BASE_URL + BackendUrls.CATEGORIES, [
      category,
    ]);
  }

  updateCategory(category: RowCategory) {
    return this.http.put<any>(
      BackendUrls.BASE_URL + BackendUrls.CATEGORIES + '/category',
      category
    );
  }

  deleteCategory(id: number) {
    return this.http.delete(
      BackendUrls.BASE_URL + BackendUrls.CATEGORIES + '/category/' + id
    );
  }
}
