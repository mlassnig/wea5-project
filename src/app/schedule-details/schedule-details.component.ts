import { Component, Input, OnInit } from '@angular/core';
import { ScheduleDto } from '../interfaces/dto/schedule.dto.type';

@Component({
  selector: 'schedule-details',
  templateUrl: './schedule-details.component.html',
  styleUrls: ['./schedule-details.component.scss'],
})
export class ScheduleDetailsComponent implements OnInit {
  @Input() schedule: ScheduleDto | undefined;

  constructor() {}

  ngOnInit(): void {}
}
