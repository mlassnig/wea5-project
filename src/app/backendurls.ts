export class BackendUrls {
    private static readonly PORT: string = '5000';
    public static readonly BASE_URL: string = 'http://localhost:' + BackendUrls.PORT + '/api';
    public static readonly MOVIES: string =  '/movies';
    public static readonly SCHEDULES: string = '/schedules';
    public static readonly ACTORS: string = '/actors';
    public static readonly CATEGORIES: string = '/categories';
    public static readonly GENRES: string = '/genres';
    public static readonly CINEMAHALLS: string = '/cinemaHalls';
}