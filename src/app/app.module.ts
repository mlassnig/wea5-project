import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CinemaProgramComponent } from './cinema-program/cinema-program.component';
import { HeaderComponent } from './partials/header/header.component';
import { MovieLibraryComponent } from './movie-library/movie-library.component';
import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ScheduleDetailsComponent } from './schedule-details/schedule-details.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminAreaComponent } from './admin-area/admin-area.component';
import { MovieListComponent } from './movie-list/movie-list.component';
import { CinemaHallListComponent } from './cinema-hall-list/cinema-hall-list.component';
import { SchedulesListComponent } from './schedules-list/schedules-list.component';
import { AdminMovieDetailsComponent } from './admin-movie-details/admin-movie-details.component';
import { AdminScheduleDetailsComponent } from './admin-schedule-details/admin-schedule-details.component';
import { AdminCinemaHallDetailsComponent } from './admin-cinema-hall-details/admin-cinema-hall-details.component';
import { RowCategoryListComponent } from './row-category-list/row-category-list.component';
import { AdminCategoryDetailsComponent } from './admin-category-details/admin-category-details.component';
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks';
import { OAuthModule, OAuthService } from 'angular-oauth2-oidc';
import { authConfig } from './auth.config';

@NgModule({
  declarations: [
    AppComponent,
    CinemaProgramComponent,
    HeaderComponent,
    MovieLibraryComponent,
    MovieDetailsComponent,
    ScheduleDetailsComponent,
    AdminAreaComponent,
    MovieListComponent,
    CinemaHallListComponent,
    SchedulesListComponent,
    AdminMovieDetailsComponent,
    AdminScheduleDetailsComponent,
    AdminCinemaHallDetailsComponent,
    RowCategoryListComponent,
    AdminCategoryDetailsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    OAuthModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(private oauthService: OAuthService) {
    this.configureWithNewConfigApi();
  }

  private configureWithNewConfigApi() {
    this.oauthService.configure(authConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
  }
}
