export interface RowCategory {
  id: number;
  name: string;
  price: number;
}
