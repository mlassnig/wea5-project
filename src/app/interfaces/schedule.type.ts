import { Movie } from './movie.type';
import { CinemaHall } from './cinemaHall.type';

export interface Schedule {
  startTime: number;
  movieRef: Movie;
  cinemaHallRef: CinemaHall;
}
