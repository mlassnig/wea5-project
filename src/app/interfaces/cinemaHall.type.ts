import { SeatRow } from './seatRow.type';

export interface CinemaHall {
  id: number;
  name: string;
  rows: SeatRow[];
}
