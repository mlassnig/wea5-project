import { CinemaHall } from './cinemaHall.type';
import { RowCategory } from './rowCategory.type';
import { Seat } from './seat.type';

export interface SeatRow {
  rowCategory: RowCategory;
  seats: Seat[];
}
