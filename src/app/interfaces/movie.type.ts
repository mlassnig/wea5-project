import { Actor } from './actor.type';
import { Genre } from './genre.type';

export interface Movie {
  id: number;
  title: string;
  length: number;
  image: string;
  trailerLink: string;
  description: string;
}
