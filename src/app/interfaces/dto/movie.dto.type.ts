import { Movie } from '../movie.type';
import { Actor } from './../actor.type';
import { Genre } from './../genre.type';

export interface MovieDto {
  actors: Actor[];
  genres: Genre[];
  movie: Movie;
}
