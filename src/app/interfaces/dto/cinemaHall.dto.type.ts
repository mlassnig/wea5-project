import { CinemaHall } from '../cinemaHall.type';
import { SeatRowDto } from './seatRow.dto.type';

export interface CinemaHallDto {
  cinemaHall: CinemaHall;
  numberOfSeats: number;
  rows: SeatRowDto[];
}
