import { Movie } from '../movie.type';
import { Schedule } from '../schedule.type';
import { Actor } from './../actor.type';
import { Genre } from './../genre.type';

export interface ScheduleDto {
  schedule: Schedule;
  percentageTakenSeats: number;
}
