import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';
import { MovieDto } from '../interfaces/dto/movie.dto.type';
import { faEdit, faTrash, faPlus } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import * as _ from 'lodash';

@Component({
  selector: 'movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
})
export class MovieListComponent implements OnInit {
  movies: MovieDto[] = undefined!;
  selectedMovie: MovieDto = undefined!;
  isInitialized: boolean = false;
  faEdit = faEdit;
  faTrash = faTrash;
  faPlus = faPlus;
  constructor(private backendService: BackendService, private router: Router) {}

  ngOnInit(): void {
    this.backendService.getMovies().subscribe((data) => {
      this.movies = data;
      this.isInitialized = true;
    });
  }

  onMovieSelect(movie: MovieDto): void {
    this.selectedMovie = movie;
  }

  deleteMovie(): void {
    if (this.selectedMovie.movie && this.selectedMovie.movie.id > 0) {
      this.backendService
        .deleteMovie(this.selectedMovie.movie.id)
        .subscribe((response) => {
          _.remove(this.movies, (m) => {
            return m.movie.id === this.selectedMovie.movie.id;
          });
        });
    }
  }

  editMovie(movie: MovieDto): void {
    this.router.navigate(['/admin/movieDetails/' + movie.movie.id]);
  }
}
