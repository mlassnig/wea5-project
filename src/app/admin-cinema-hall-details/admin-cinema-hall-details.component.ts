import { unescapeIdentifier } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BackendService } from '../backend.service';
import { CinemaHallDto } from '../interfaces/dto/cinemaHall.dto.type';
import { RowCategory } from '../interfaces/rowCategory.type';
import * as _ from 'lodash';
import { SeatRowDto } from '../interfaces/dto/seatRow.dto.type';
import { SeatRow } from '../interfaces/seatRow.type';

@Component({
  selector: 'admin-cinema-hall-details',
  templateUrl: './admin-cinema-hall-details.component.html',
  styleUrls: ['./admin-cinema-hall-details.component.scss'],
})
export class AdminCinemaHallDetailsComponent implements OnInit {
  private inputCinemaHall: CinemaHallDto;
  allCategories: RowCategory[];
  selectedCategories: RowCategory[];
  categoryCount: number[];
  isActionCreate: boolean = true;
  form: FormGroup;
  constructor(
    private backendService: BackendService,
    private route: ActivatedRoute,
    private router: Router,
    formBuilder: FormBuilder
  ) {
    this.categoryCount = [1];
    this.inputCinemaHall = undefined!;
    this.allCategories = undefined!;
    this.selectedCategories = new Array<RowCategory>();
    this.selectedCategories.pop();

    this.form = formBuilder.group({
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(45), //https://www.imdb.com/list/ls064443882/
        ],
      ],
      numOfSeatsPerRow: [
        '',
        [Validators.required, Validators.min(1), Validators.max(30)],
      ],
    });
  }

  ngOnInit(): void {
    let id: number = this.route.snapshot.params['id'];
    if (id > 0) {
      this.backendService.getCinemaHallById(id).subscribe((data) => {
        this.isActionCreate = false;
        this.inputCinemaHall = data;
        this.form.controls['name'].setValue(
          this.inputCinemaHall.cinemaHall.name
        );
        this.form.controls['numOfSeatsPerRow'].setValue(
          this.inputCinemaHall.numberOfSeats
        );
      });
    }

    this.backendService.getCategories().subscribe((data) => {
      this.allCategories = data;
    });
  }

  addCategory() {
    this.categoryCount.push(this.categoryCount.length + 1);
  }
  onRowSelectChange(categoryName: string, index: number) {
    let selectedCategory = _.filter(this.allCategories, (c) => {
      return c.name === categoryName;
    })[0];
    this.selectedCategories[index - 1] = selectedCategory;
  }

  onSubmit() {
    let rows: SeatRowDto[] = new Array<SeatRowDto>();
    _.forEach(this.selectedCategories, (category) => {
      rows.push({ categoryId: category.id });
    });
    let newHall: CinemaHallDto = {
      cinemaHall: {
        id: 0,
        name: this.form.controls['name'].value,
        rows: undefined!,
      },
      numberOfSeats: this.form.controls['numOfSeatsPerRow'].value,
      rows,
    };
    if (!this.isActionCreate) {
      newHall.cinemaHall.id = this.inputCinemaHall.cinemaHall.id;
      this.backendService.updateCinemaHall(newHall).subscribe(
        (res) => {
          this.router.navigate(['/admin']);
        },
        (error) => {
          console.log(error);
        }
      );
    } else {
      this.backendService.insertCinemaHall(newHall).subscribe(
        (res) => {
          this.router.navigate(['/admin']);
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }
}
