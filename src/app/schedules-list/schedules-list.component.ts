import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';
import { ScheduleDto } from '../interfaces/dto/schedule.dto.type';
import { faEdit, faTrash, faPlus } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { DataStoreService } from '../data-store.service';
import * as _ from 'lodash';

@Component({
  selector: 'schedules-list',
  templateUrl: './schedules-list.component.html',
  styleUrls: ['./schedules-list.component.scss'],
})
export class SchedulesListComponent implements OnInit {
  faEdit = faEdit;
  faTrash = faTrash;
  faPlus = faPlus;
  isInitialized: boolean = false;
  errors: string[] = new Array<string>();
  success: string[] = new Array<string>();
  schedules: ScheduleDto[] = undefined!;
  selectedSchedule: ScheduleDto = undefined!;
  constructor(
    private backendService: BackendService,
    private router: Router,
    private dataStore: DataStoreService
  ) {}

  ngOnInit(): void {
    this.backendService.getSchedules().subscribe((data) => {
      this.schedules = data;
      this.isInitialized = true;
    });
  }

  onScheduleSelect(schedule: ScheduleDto): void {
    this.selectedSchedule = schedule;
  }

  deleteSchedule(): void {
    this.errors = [];
    this.success = [];
    if (this.selectedSchedule) {
      this.backendService.deleteSchedule(this.selectedSchedule).subscribe(
        () => {
          this.success.push('Successfully deleted schedule.');
          _.remove(this.schedules, (s) => {
            return (
              s.schedule.startTime ===
                this.selectedSchedule.schedule.startTime &&
              s.schedule.cinemaHallRef.id ===
                this.selectedSchedule.schedule.cinemaHallRef.id
            );
          });
        },
        (error) => {
          this.errors.push(error.error);
        }
      );
    }
  }

  editSchedule(schedule: ScheduleDto): void {
    this.dataStore.set(schedule, 'schedule');
    this.router.navigate(['/admin/scheduleDetails/']);
  }
}
