import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'admin-area',
  templateUrl: './admin-area.component.html',
  styleUrls: ['./admin-area.component.scss'],
})
export class AdminAreaComponent implements OnInit {
  selectedTab: string = 'Movies';

  constructor() {}

  ngOnInit(): void {}

  onTabSelect(selected: string): void {
    this.selectedTab = selected;
  }
}
