import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BackendService } from '../backend.service';
import { RowCategory } from '../interfaces/rowCategory.type';

@Component({
  selector: 'admin-category-details',
  templateUrl: './admin-category-details.component.html',
  styleUrls: ['./admin-category-details.component.scss'],
})
export class AdminCategoryDetailsComponent implements OnInit {
  isInitialized: boolean = true;
  isActionCreate: boolean = true;
  errors: string[] = new Array<string>();
  inputCategory: RowCategory = undefined!;
  form: FormGroup;
  constructor(
    private backendService: BackendService,
    private route: ActivatedRoute,
    private router: Router,
    formBuilder: FormBuilder
  ) {
    this.form = formBuilder.group({
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(30),
        ],
      ],
      price: ['', [Validators.required, Validators.min(0.01)]],
    });
  }

  ngOnInit(): void {
    let id = this.route.snapshot.params['id'] || 0;
    if (id > 0) {
      this.backendService.getCategoryById(id).subscribe((data) => {
        this.inputCategory = data;
        this.isInitialized = true;
        this.isActionCreate = false;
        this.form.controls['name'].setValue(this.inputCategory.name);
        this.form.controls['price'].setValue(this.inputCategory.price);
      });
    }
  }

  onSubmit(): void {
    if (this.form.valid) {
      let newCategory: RowCategory = {
        id: 0,
        name: this.form.controls['name'].value,
        price: this.form.controls['price'].value,
      };

      if (!this.isActionCreate) {
        newCategory.id = this.inputCategory.id;
        this.backendService.updateCategory(newCategory).subscribe(
          (response) => {
            this.router.navigate(['/admin']);
          },
          (error) => {
            this.errors.push(error.error);
          }
        );
      } else {
        this.backendService.insertCategory(newCategory).subscribe(
          (response) => {
            this.router.navigate(['/admin']);
          },
          (error) => {
            this.errors.push(error.error);
          }
        );
      }
    }
  }
}
