import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DataStoreService {
  private data: Map<string, any>;
  constructor() {
    this.data = new Map<string, any>();
  }

  set(object: any, index: string): void {
    this.data.set(index, object);
  }

  get(index: string): any {
    return this.data.get(index);
  }
}
