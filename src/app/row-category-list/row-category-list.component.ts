import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faEdit, faTrash, faPlus } from '@fortawesome/free-solid-svg-icons';
import { BackendService } from '../backend.service';
import { RowCategory } from '../interfaces/rowCategory.type';
import * as _ from 'lodash';

@Component({
  selector: 'row-category-list',
  templateUrl: './row-category-list.component.html',
  styleUrls: ['./row-category-list.component.scss'],
})
export class RowCategoryListComponent implements OnInit {
  faPlus = faPlus;
  faTrash = faTrash;
  faEdit = faEdit;
  categories: RowCategory[] = new Array<RowCategory>();
  selectedCategory: RowCategory = undefined!;
  errors: string[] = new Array<string>();
  success: string[] = new Array<string>();

  isInitialized: boolean = false;

  constructor(private backendService: BackendService, private router: Router) {}

  ngOnInit(): void {
    this.backendService.getCategories().subscribe((data) => {
      this.categories = data;
      this.isInitialized = true;
    });
  }

  onCategorySelect(category: RowCategory) {
    this.selectedCategory = category;
  }

  editCategory(category: RowCategory) {
    this.router.navigate(['/admin/categoryDetails/' + category.id]);
  }

  deleteCategory() {
    if (this.selectedCategory && this.selectedCategory.id > 0) {
      this.backendService.deleteCategory(this.selectedCategory.id).subscribe(
        (response) => {
          this.success.push('Successfully deleted category.');
          _.remove(this.categories, (category) => {
            return category.id === this.selectedCategory.id;
          });
        },
        (error) => {
          this.errors.push(error.error);
        }
      );
    }
  }
}
