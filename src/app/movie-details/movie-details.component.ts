import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BackendService } from '../backend.service';
import { MovieDto } from '../interfaces/dto/movie.dto.type';
import * as _ from 'lodash';
import { ScheduleDto } from '../interfaces/dto/schedule.dto.type';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss'],
})
export class MovieDetailsComponent implements OnInit {
  isInitialized: boolean = false;
  movie: MovieDto = undefined!;
  schedules: ScheduleDto[] = undefined!;

  constructor(
    private route: ActivatedRoute,
    private backendService: BackendService
  ) {}

  ngOnInit() {
    let id = this.route.snapshot.params['id'];
    let isMovieInitialized = false;
    let isSchedulesInitialized = false;
    this.backendService.getMovieById(id).subscribe((data: MovieDto) => {
      this.movie = data;
      isMovieInitialized = true;
      this.isInitialized = isMovieInitialized && isSchedulesInitialized;
      if (!this.movie.movie.trailerLink) {
        this.movie.movie.trailerLink =
          'https://www.youtube.com/watch?v=w3PP5KfQlO8&ab_channel=MovieGasm.com';
      }
    });

    this.backendService
      .getSchedulesForMovie(id)
      .subscribe((data: ScheduleDto[]) => {
        this.schedules = data;
        isSchedulesInitialized = true;
        this.isInitialized = isMovieInitialized && isSchedulesInitialized;
      });
  }
}
