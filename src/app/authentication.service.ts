import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(private oauthService: OAuthService, private router: Router) {}

  login(): boolean {
    this.oauthService.initImplicitFlow();
    return true;
  }

  isLoggedIn(): boolean {
    return (
      this.oauthService.hasValidAccessToken() &&
      this.oauthService.hasValidIdToken()
    );
  }

  logout(): void {
    this.oauthService.logOut();
    this.router.navigate(['/']);
  }
}
