import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';
import { CinemaHallDto } from '../interfaces/dto/cinemaHall.dto.type';
import { faEdit, faTrash, faPlus } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import * as _ from 'lodash';

@Component({
  selector: 'cinema-hall-list',
  templateUrl: './cinema-hall-list.component.html',
  styleUrls: ['./cinema-hall-list.component.scss'],
})
export class CinemaHallListComponent implements OnInit {
  faEdit = faEdit;
  faTrash = faTrash;
  faPlus = faPlus;

  errors: string[];
  success: string[];
  isInitialized: boolean = false;
  selectedCinemaHall: CinemaHallDto;
  cinemaHalls: CinemaHallDto[] = new Array<CinemaHallDto>();

  constructor(private backendService: BackendService, private router: Router) {
    this.selectedCinemaHall = undefined!;
    this.errors = new Array<string>();
    this.success = new Array<string>();
  }

  ngOnInit(): void {
    this.backendService.getCinemaHalls().subscribe((data) => {
      this.cinemaHalls = data;
      this.isInitialized = true;
    });
  }

  onHallSelect(hall: CinemaHallDto): void {
    this.selectedCinemaHall = hall;
  }

  deleteHall(): void {
    if (this.selectedCinemaHall && this.selectedCinemaHall.cinemaHall.id > 0) {
      this.backendService
        .deleteCinemaHall(this.selectedCinemaHall.cinemaHall.id)
        .subscribe(
          (response) => {
            this.success.push('Successfully deleted cinema hall.');
            _.remove(this.cinemaHalls, (hall) => {
              return (
                hall.cinemaHall.id === this.selectedCinemaHall.cinemaHall.id
              );
            });
          },
          (error) => {
            this.errors.push(error.error);
          }
        );
    }
  }

  editHall(cinemaHall: CinemaHallDto): void {
    this.router.navigate([
      '/admin/cinemaHallDetails/' + cinemaHall.cinemaHall.id,
    ]);
  }
}
