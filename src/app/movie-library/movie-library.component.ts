import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';
import { MovieDto } from '../interfaces/dto/movie.dto.type';
import * as _ from 'lodash';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-movie-library',
  templateUrl: './movie-library.component.html',
  styleUrls: ['./movie-library.component.scss'],
})
export class MovieLibraryComponent implements OnInit {
  faPlus = faPlus;

  isInitialized: boolean = false;
  movies: MovieDto[] = new Array<MovieDto>();
  constructor(private backendService: BackendService) {}

  async ngOnInit(): Promise<void> {
    this.backendService.getMovies().subscribe((data: MovieDto[]) => {
      this.movies = data;
      this.isInitialized = true;
    });
  }
}
